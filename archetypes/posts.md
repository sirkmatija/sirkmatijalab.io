+++
title = "{{ replace .TranslationBaseName "-" " " | title }}"
date = "{{ .Date }}"
author = "Matija Sirk"
cover = ""
tags = [""]
keywords = [""]
description = ""
showFullContent = false
readingTime = false
hideComments = true
+++

import LichessPgnViewer from "lichess-pgn-viewer";

window.addEventListener("load", function () {
  [...this.document.getElementsByClassName("pgn-shortcode")].forEach(
    (pgn_container) => {
      const pgn = pgn_container.dataset.pgn.trim();

      LichessPgnViewer(pgn_container, {
        pgn,
      }).goTo("first");
    }
  );

  [...this.document.getElementsByClassName("fen-shortcode")].forEach(
    (fen_container) => {
      const fen = fen_container.dataset.fen.trim();

      console.log(fen);

      LichessPgnViewer(fen_container, {
        fen,
      }).goTo("first");
    }
  );
});

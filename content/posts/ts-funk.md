+++
title = "Manjkajoče typescript funkcije"
date = "2023-07-11T09:02:03+02:00"
author = "Matija Sirk"
cover = ""
tags = ["Kompjuter"]
keywords = ["typescript", "FOO"]
description = "Funkcijsko orientirano programiranje ima v typescriptu dobro podporo. A par funkcij manjka."
showFullContent = false
readingTime = false
hideComments = true
+++

Implementacije so optimizirane za berljivost ne hitrost.

## zip()

`zip()` vzame dva seznama in vrne nov seznam parov, kjer je prvi element 
v paru iz prvega seznama, drugi je pa soležni v drugem seznamu.

```typescript
export function zip<T, H>(fst: T[], snd: H[]): [T, H][] {
  return fst.map((val, index) => [val, snd[index]]);
}
```

### Uporaba v praksi

Uporabimo ga lahko za pretvorbo iz `object of arrays` vzorca v `array of objects` ali `record` vzorec.

```typescript
const objectOfArrays = {
  names: ["Žak", "Pat", "Mat"],
  heights: [165, 163, 172],
};

const entries = zip(objectOfArrays.names, objectOfArrays.heights);

const arrayOfObjects = entries.map(([name, height]) => ({ name, height }));
// -> [{name: "Žak", height: 165}, ...]

const record = Object.fromEntries(entries);
// -> { Žak: 165, Pat: 163, Mat: 172 }
```

## range()

Pogosto rabimo vsa števila do *n* (0, 1, 2, ..., n-1) ali pa vsa števila med 
*n* in *m* (n, n+1, n+2, ..., m-1) ali pa vsa soda števila do *2n* (2, 
4, ..., 2n-2) ali ... Vsem je skupen vzorec, da štejemo od nečesa do nečesa 
za korak, oziroma vmes apliciramo še trasnformacijo.

Takšno funkcijo ni težko napisati, je pa fino, da je variadiačna, da ni
potrebe po stalnem ponavljanju nepotrebnih parametrov. Zato moramo poseči
po [overload signatures](https://www.typescriptlang.org/docs/handbook/2/functions.html#function-overloads).

```typescript
export function range(to: number): number[];
export function range(from: number, to: number): number[];
export function range(from: number, to: number, step: number): number[];
export function range<T>(
  from: number,
  to: number,
  step: number,
  generator: (value: number, index: number) => T
): T[];
export function range<T>(
  fromOrTo: number,
  to?: number,
  step?: number,
  generator?: (value: number, index: number) => T
) {
  const myFrom = to === undefined ? 0 : fromOrTo;
  const myTo = to === undefined ? fromOrTo : to;
  const myStep = step ?? 1;

  let acc: number[] = [];

  for (let i = myFrom; i < myTo; i += myStep) {
    acc.push(i);
  }

  if (generator !== undefined) {
    return acc.map(generator);
  } else {
    return acc;
  }
}
```

### Uporaba v praksi

Generiranje oznak na abscisi grafa v visualizacijah:

```tsx
<XAxis
  dataKey="x"
  type="number"
  allowDecimals={false}
  ticks={range(0, 8.5, 0.5)}
  unit="h"
/>
```

Generiranje *mock* podatkov za simulacijo:

```typescript
const measures = range(1, 1001, 1, (id) => ({
    id,
    measure: Math.random()
})) // => [{ id: 1, measure: 0.6542 }, ...]
```

Tvorba časovnih intervalov:

```typescript
const dateIntervals = range(0, 10, 1, (deltaHours) => ({
    from: new Date(2023, 6, 10, 6 + deltaHours),
    to: new Date(2023, 6, 10, 7 + deltaHours)
}))
```

## chunk()

Razbije seznam na več enako velikih kosov (kjer je zadnji kos morda manjši).

```typescript
export function chunk<T>(list: T[], chunkSize: number) {
  const numChunks = Math.ceil(list.length / chunkSize);

  const chunks = new Array(numChunks)
    .fill(null)
    .map((_, i) => list.slice(i * chunkSize, (i + 1) * chunkSize));

  return chunks;
}
```

### Uporaba v praksi

Prikaz seznama v več vrsticah:

```tsx
const data = [1, 2, 3, 4, 5, 6, 7, 8];

const rows = chunk(data, 2);

return (
  <div className="grid">
    {rows.map((chnk, i) => (
      <div key={i} className="row">
        {chnk.map((dp) => (
          <span key={dp} className="cell">{dp}</span>
        ))}
      </div>
    ))}
  </div>
);
```

## groupBy()

Objekte v seznamu moramo pogosto grupirati glede na določeno lastnost. Npr. v cenovne razrede, datum nastanka
ali status. V tem primeru prav pride `groupBy`. Ta vzame seznam in funkcijo, ki vsakemu elemento določi oznako.
Vrne pa seznam oznak in njihovih pripadnikov.

```typescript
export function groupBy<T, Label extends string>(
  array: T[],
  groupFn: (arg: T) => Label
): { group: Label; members: T[] }[] {
  const withGroup = array.map((member) => ({ group: groupFn(member), member }));

  const groups = [...new Set(withGroup.map(({ group }) => group))];

  return groups.map((group) => ({
    group,
    members: withGroup
      .filter((g) => g.group === group)
      .map(({ member }) => member),
  }));
}
```

### Uporaba v praksi

Grupiranje artiklov po ceni:

```typescript
const items = [
  { id: 1, price: 10 },
  { id: 2, price: 15 },
  { id: 3, price: 25 },
  { id: 4, price: 4 },
];

function labelItem(item: (typeof items)[number]) {
  if (item.price < 10) {
    return "Cheap";
  } else if (item.price < 20) {
    return "Okayish";
  } else {
    return "Expensive";
  }
}

const priceGroups = groupBy(items, labelItem);
```

## partition()

Dostikrat pa ne rabimo več skupin, a le razdeliti seznam na dvoje glede na kriterij. Torej nekaj podobnega
[Array.prototype.filter()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter), kar bo vrnilo tako ujemajoče kot neujemajoče elemente.

```typescript
export function partition<T>(
  array: T[],
  predicate: (arg: T) => boolean
): [T[], T[]] {
  const truish = array.filter(predicate);
  const falsish = array.filter((arg) => !predicate(arg));

  return [truish, falsish];
}
```

### Uporaba v praksi

Razdeli seznam na elemente s pozitivnim in negativnim trendom:

```typescript
const data = [
  { id: 1, trend: -2 },
  { id: 2, trend: +1 },
  { id: 3, trend: -0.55 }, 
  ...
];

const [positiveTrend, negativeTrend] = partition(
  data,
  ({ trend }) => trend > 0
);
```
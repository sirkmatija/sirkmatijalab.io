+++
title = "Q_rsqrt v WASM"
date = "2023-07-10T13:41:31+02:00"
author = "Matija Sirk"
cover = ""
tags = ["Kompjuter"]
keywords = ["Quake", "WASM", "sqrt"]
description = "Quake-ov hitri inverz korena v WASM."
showFullContent = false
readingTime = false
hideComments = true
toc = false
+++

## Q_rsqrt

```
;; Izvirna C koda: Quake 3 Arena via https://en.wikipedia.org/wiki/Fast_inverse_square_root#Overview_of_the_code

(module 
    ;; float Q_rsqrt( float number )
    ;; {
    ;;     long i;
    ;;     float x2, y;
    ;;     const float threehalfs = 1.5F;
    (func (export "Q_rsqrt") (param $number f32) (result f32)   ;; Function signature.
        (local $i i32) (local $x2 f32) (local $y f32)           ;; Local vars need to be declared at beginning of function.

        ;; x2 = number * 0.5F;
        local.get $number                                       ;; Push parameter to stack.
        f32.const 0.5                                           ;; Push constant to stack.
        f32.mul                                                 ;; Pop 2 values from stack and push multiplied result to stack.
        local.set $x2                                           ;; Pop result of multiply from stack and assign it to local var.

        ;; y  = number;
        local.get $number
        local.set $y

        ;; i  = * ( long * ) &y;                       // evil floating point bit level hacking
        local.get $y
        i32.reinterpret_f32                                     ;; Funky operation. Reinterprets bits of f32 (C float) value as i32 (C long) value.
        local.set $i

        ;; i  = 0x5f3759df - ( i >> 1 );               // what the fuck? 
        i32.const 0x5f3759df
        local.get $i
        i32.const 1
        i32.shr_s                                               ;; Shift right signed. In WASM numbers aren't signed or unsigned, operations are.
        i32.sub                                                 ;; Pops two values and substracts second from first.
        local.set $i

        ;; y  = * ( float * ) &i;
        local.get $i
        f32.reinterpret_i32                                     ;; Analogous to i32.reinterpret_f32
        local.set $y

        ;; y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
        local.get $y
        f32.const 1.5
        local.get $x2
        local.get $y
        f32.mul
        local.get $y
        f32.mul
        f32.sub
        f32.mul
        local.set $y

        ;; //    y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed

        ;; return y;
        local.get $y
))
```

## Preizkus

```js
const wasmInstance = new WebAssembly.Instance(wasmModule, {});
const { Q_rsqrt } = wasmInstance.exports;

new Array(20)
  .fill(null)
  .map((_, i) => i)
  .forEach((num) => {
    console.log(Q_rsqrt(num), " ", 1 / Math.sqrt(num));
  });
```
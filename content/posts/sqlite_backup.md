+++
title = "Kako skopirati SQLite bazo?"
date = "2023-07-10T13:56:36+02:00"
author = "Matija Sirk"
cover = ""
tags = ["Kompjuter"]
keywords = ["SQLite", "Python3", "backup"]
description = "Inkrementalni backup SQLite baze."
showFullContent = false
readingTime = false
hideComments = true
toc = false
+++

Iz dokumentacije:

> Historically, backups (copies) of SQLite databases have been created using the following method:
> 
> Establish a shared lock on the database file using the SQLite API (i.e. the shell tool).
> Copy the database file using an external tool (for example the unix 'cp' utility or the DOS 'copy' command).
> Relinquish the shared lock on the database file obtained in step 1.
> This procedure works well in many scenarios and is usually very fast. However, this technique has the following shortcomings:
> 
> Any database clients wishing to write to the database file while a backup is being created must wait until the shared lock is relinquished.
> It cannot be used to copy data to or from in-memory databases.
> If a power failure or operating system failure occurs while copying the database file the backup database may be corrupted following system recovery.
> The Online Backup API was created to address these concerns. The online backup API allows the contents of one database to be copied into another database 
> file, replacing any original contents of the target database. The copy operation may be done incrementally, in which case the source database does not
> need to be locked for the duration of the copy, only for the brief periods of time when it is actually being read from. This allows other database users
> to continue without excessive delays while a backup of an online database is made.
> - [SQLite: using the SQLite Online Backup API](https://www.sqlite.org/backup.html)

```python
import datetime
import sqlite3
import sys
from pathlib import Path

DB_DIR = Path(sys.argv[1])
BACKUP_DIR = Path(sys.argv[2])

date = datetime.datetime.now()
date_string = date.isoformat().split(".")[0].replace(":", "-")

for original_file in DB_DIR.glob("*.sqlite3"):
    original_name = original_file.stem
    backup_name = f"{original_name}_{date_string}.sqlite3"

    backup_file = BACKUP_DIR / backup_name
    backup_file.touch()

    original_db = sqlite3.connect(original_file)
    backup_db = sqlite3.connect(backup_file)

    with backup_db:
        original_db.backup(backup_db, pages=1)

    original_db.close()
    backup_db.close()

```
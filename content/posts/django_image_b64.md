+++
title = "Kako shraniti base64 sliko v Djangov ImageField?"
date = "2023-07-10T14:03:45+02:00"
author = "Matija Sirk"
cover = ""
tags = ["Kompjuter"]
keywords = ["Django", "Python3", "Base64"]
description = "All your base are belong to us."
showFullContent = false
readingTime = false
hideComments = true
toc = false
+++

```python
import base64
from django.core.files.base import ContentFile

from .models import SomeModel

def parse_base64(b64: str, file_name: str) -> ContentFile:
    format, content = b64.split(';base64,')

    ext = format.split("/")[-1]
    name = f"{file_name}.{ext}"

    file = ContentFile(base64.b64decode(content), name=name)

    return file

image = parse_base64("base64string", "name")

model = SomeModel(image=image)

```
+++
title = "useAnimationFrame()"
date = "2023-07-11T08:18:09+02:00"
author = "Matija Sirk"
cover = ""
tags = ["Kompjuter"]
keywords = ["typescript", "React", "hooks"]
description = "Kako klicati funkcijo pred vsakim risanjem okna?"
showFullContent = false
readingTime = false
hideComments = true
toc = false
+++

Iz dokumentacije:

> The window.requestAnimationFrame() method tells the browser that you wish to perform an animation and requests that the browser calls a 
> specified function to update an animation right before the next repaint. The method takes a callback as an argument to be invoked before the repaint.
> - [mdn: Window: requestAnimationFrame() method](https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame)

Da ni animacija groba mora funkcija upoštevati pretečeni čas od zadnjega risanja, saj ni nujno vedno enak. Zato funkciji damo kot argument 
delto, to je pretečeni čas od zadnjega klica.

```typescript
import { useEffect, useRef } from "react";

// Reference: https://css-tricks.com/using-requestanimationframe-with-react-hooks/

export function useAnimationFrame(callback: (delta: number) => void) {
  const requestRef = useRef<number>();
  const previousTimeRef = useRef<number>();

  function animate(time: number) {
    if (previousTimeRef.current != undefined) {
      const deltaTime = time - previousTimeRef.current;

      callback(deltaTime);
    }

    previousTimeRef.current = time;
    requestRef.current = requestAnimationFrame(animate);
  }

  useEffect(() => {
    requestRef.current = requestAnimationFrame(animate);

    return () => {
      if (requestRef.current !== undefined) {
        cancelAnimationFrame(requestRef.current);
      }
    };
  }, []); 
```
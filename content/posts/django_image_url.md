+++
title = "Kako shraniti sliko iz url-ja v Djangov ImageField?"
date = "2023-07-10T14:06:00+02:00"
author = "Matija Sirk"
cover = ""
tags = ["Kompjuter"]
keywords = ["Django", "Python3", "requests"]
description = "Ko klient ne POST-a pač GET-amo."
showFullContent = false
readingTime = false
hideComments = true
toc = false
+++

```python
import requests
from django.core.files.base import ContentFile

from .models import SomeModel

image_r = requests.get(f"https://www.example.org/someimage.png")

image = ContentFile(image_r.content, name="name.png") if image_r.ok else None

model = SomeModel(image=image)
```